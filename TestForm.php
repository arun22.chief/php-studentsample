<!DOCTYPE HTML>
<html>

<head>
  <style>
    .error {color: #FF0000;}
</style>
</head>

<body>

  <?php
// define variables and set to empty values
$nameErr = $emailErr = $genderErr ="";
$name = $email = $gender = $comment ="";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  // this section gets activated, when form is posted
    $name = $_POST["name"];

    $email = $_POST["email"];
 
    $comment = "";

    $gender = $_POST["gender"];


      echo "<h2>Your Input:</h2>";
      echo $name;
      echo "<br>";
      echo $email;
      echo "<br>";
      echo $comment;
      echo "<br>";
      echo $gender;


      $conn = mysqli_connect('localhost','root','','crud');  // conneting to database

      //inserting data
      if (!$conn) {  //checking if connection is present
          die('Could not connect: ' . mysqli_error($conn));  // if not working, shows error
      }
      else{
            $sql = "INSERT INTO `customer` (`ID`, `Name`, `age`) VALUES (NULL, '$name', '12')";  // if connection is ok, then this querty string is executed below

            if ($conn->query($sql) === TRUE) {  //executing our query
                echo "<br>New record created successfully<br>";
                echo "Affected rows: " . mysqli_affected_rows($conn)."<br>";
            } 
            else { 
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
      }
      
      //dont close the connection here

      //reading data
      $sql = "SELECT `ID`, `Name`, `age` FROM `customer` WHERE `age` > 10";  // our query
      $result = $conn->query($sql);  //executing the query

      if ($result->num_rows > 0) {  //if result has more than one row
        
          while($row = $result->fetch_assoc()) {   //looping through each row
              echo "id: " . $row["ID"]. " - Name: " . $row["Name"]. "<br>";  
          }

          echo "Affected rows: " . mysqli_affected_rows($conn);

      } else {
          echo "0 results";
      }


      //update data
      $sql = "UPDATE customer SET `Name`='Doe' WHERE age=2";

      if ($conn->query($sql) === TRUE) {
          echo "<br>Record updated successfully<br>";  //means query was successful, does not mean any rows are updated
          echo "Affected rows: " . mysqli_affected_rows($conn);
      } else {
          echo "Error updating record: " . $conn->error;
      }


      $conn->close();


}


?>



  <h2>HTML form only for posting data</h2>

  <form method="post" action="">
    Name: <input type="text" required name="name">
    <br><br>
    E-mail: <input type="text" name="email">
    <br><br>
    Comment: <textarea name="comment" rows="5" cols="40"></textarea>
    <br><br>
    Gender:
    <input type="radio" required name="gender" value="female">Female
    <input type="radio" name="gender" value="male">Male
    <input type="radio" name="gender" value="other">Other
    <br><br>
    <input type="submit" name="submit" value="Submit">
  </form>

  <br>
  <br>

  <h2>PHP Form (for edit purpose, data is again loaded to form inputs)</h2>

  <form method="post" action="">
    Name: <input type="text" required name="name" value="<?php echo $name;?>">
    <br><br>
    E-mail: <input type="text" name="email" value="<?php echo $email;?>">
    <br><br>
    Comment: <textarea name="comment" rows="5" cols="40"><?php echo $comment;?></textarea>
    <br><br>
    Gender:
    <input type="radio" required name="gender" <?php if (isset($gender) && $gender=="female" ) echo "checked" ;?>
    value="female">Female
    <input type="radio" name="gender" <?php if (isset($gender) && $gender=="male" ) echo "checked" ;?>
    value="male">Male
    <input type="radio" name="gender" <?php if (isset($gender) && $gender=="other" ) echo "checked" ;?>
    value="other">Other
    <br><br>
    <input type="submit" name="submit" value="Submit">
  </form>



</body>

</html>